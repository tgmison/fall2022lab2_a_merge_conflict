package Application;

import vehicles.Bicycle;
/**
 * Bike Store Class that holds the information of bicycle manufacturers
 * 
 * 
 * 
 * 
 * @author Tyler Mison
 * @version 9/3/2022
 */
public class BikeStore {
    /**
     * 
     * @param args
     * @return Prints Bicycle information (manufacturer, numberGears, maxSpeed)
     */

     //Main Method with Bicycle Array
    public static void main(String[] args) {
        
        Bicycle[] obj = new Bicycle[4];
        
        //Hardcode values of Bicycle objects
        obj[0] = new Bicycle("BH", 18, 62.0);
        obj[1] = new Bicycle("Claud Butler", 3, 30.0);
        obj[2] = new Bicycle("Dahon", 21, 75.0);
        obj[3] = new Bicycle("Hetchins", 24, 82.0);
        
        //Loop to cycle through array
        for(int i=0; i<obj.length; i++) {
            System.out.println(obj[i]);
         }
    }

}

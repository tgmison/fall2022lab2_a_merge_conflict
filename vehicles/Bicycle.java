/**
 * Bicycle Class with getters and setters for information on Bicycle
 * 
 * 
 * 
 * 
 * @author Tyler Mison
 * @version 9/3/2022
 */

package vehicles;

/**
 * Bicycle Class with getters and setters
 * 
 */
public class Bicycle{

    //Private Fields of Bicycle Details 1.A
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //Getters for all 3 fields PART 1.B
    public String getManufacturer() {
        return manufacturer;
    }

    public int getNumberGears() {
        return numberGears;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }
    //Constructor PART 1.C
    public Bicycle(String manufacturer, int numberGears, double maxSpeed) {
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //toString method PART 1.D
    @Override
    public String toString() {
        return "Bicycle [manufacturer = " + manufacturer + ", maxSpeed = " + maxSpeed + ", numberGears = " + numberGears
                + "]";
    }
}